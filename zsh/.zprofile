###############################################################################
#                                Env Variables                                #
###############################################################################
export EDITOR="vim"
export GOPATH="${HOME}/.local/share/go"
export TERMINAL="kitty"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"
export GOPRIVATE="wwwin-github.cisco.com/ngdevx-taas"
export PYENV_ROOT="$HOME/.pyenv"



###############################################################################
#                                 Bin Folder                                  #
###############################################################################
if [[ -e "$HOME/.local/bin" && ! $PATH =~ "$HOME/.local/bin" ]]; then
	export PATH="$HOME/.local/bin:$PATH"
fi

if [[ -e "$HOME/.cargo/bin" && ! $PATH =~ "$HOME/.cargo/bin" ]]; then
	export PATH="$HOME/.cargo/bin:$PATH"
fi

if [[ -e "$HOME/.yarn/bin" && ! $PATH =~ "$HOME/.yarn/bin" ]]; then
	export PATH="$HOME/.yarn/bin:$PATH"
fi	

if [[ -e "$GOPATH/bin" && ! $PATH =~ "$GOPATH/bin" ]]; then
	export GOBIN="${GOPATH}/bin"
	export PATH="$GOBIN:$PATH"
fi

